# iot_connect acts 指导

## 编译

### 代码复制
将当前目录下的 iotc_acts 拷贝到工程的 test/xts/acts/communication 目录下

### 新增编译测试套件
在 test/xts/acts/communication/BUILD.gn 中增加如下
```sh
"iotc_acts/iot_connect_cpp_standard:ActsIotConnectTest",
```

### 编译 acts 套件
在 test/xts/acts 目录下执行如下编译命令
```sh
./build.sh product_name=rk3568 system_size=standard
```