# acts_validator_iot

## 编译

#### 代码复制

clone 工程代码，将 validator 目录 acts_validator_iot 工程拷贝至OpenHarmony 5.0Release 标准工程的 test/xts/acts/validator目录下。

#### 新增编译脚本

将 工程目录xts_acts\validator下的BUILD.gn 文件中新增的编译节点添加在 OpenHarmony 5.0Release 标准工程的 test/xts/acts/validator/BUILD.gn 文件中

```c++
group("validator") {
    testonly = true
    if (is_standard_system) {
        deps = [ "acts_validator:acts_validator_test",
        "acts_validator_iot:acts_validator_iot_test"]
    }
}
```
#### 编译validator_iot hap

在 OpenHarmony 5.0Release 标准工程的 /test/xts/acts目录下执行下面命令编译validator_iot 测试hap。
```c++
./build.sh product_name=rk3568 system_size=standard target_subsystem=validator
```
编译完成后生成的validator_iot.hap 位于 out\rk3568\suites\haps目录下。

## 测试

#### 测试盒子镜像烧录

在社区5.0Release notes 中下载[RK3568标准系统镜像](%3Cspan%3E%3Cdiv%3Ehttps://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v5.0.0-release.md%3C/div%3E%3C/span%3E)或者后续社区发布其他Release的版本。

#### 资源安装

测试相关资源请联系OpenHarmony统一互联PMC。


上述准备工作完成后，重启设备后安装附件中的entry-default-signed_release.hap 和编译的validator_iot.hap。

#### 测试报告生成

将编译生成的acts-validator xdevice 复制到window 环境中（out\rk3568\suites\acts\acts-validator）。
打开validator_iot 应用，按照应用提示测试iot功能。测试完成后在xdevice中，执行run.bat，在窗口中输入
run validator，将validator首页生成报告按钮按一下，然后回到 xdevice 运行的bat页面点下y，报告生成后位于report 目录下。